package com.ymt.datajpa.dao;

import com.ymt.datajpa.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface IClienteDao extends CrudRepository<Cliente, Long> {

}
